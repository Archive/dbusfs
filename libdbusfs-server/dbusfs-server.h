#ifndef _libdbusfs_server_dbusfs_server_h_
#define _libdbusfs_server_dbusfs_server_h_

#include <hal/libhal.h>

struct dbusfs_device
{
  char *name;
  char *udi;

  void *private;
  struct dbusfs_device *next;
};

typedef int (*dbusfs_get_func)( struct dbusfs_device *device, const char *path,
                                char **contents, unsigned long *size );
typedef int (*dbusfs_put_func)( struct dbusfs_device *device, const char *path,
                                char *contents, unsigned long size );
typedef int (*dbusfs_rm_func)( struct dbusfs_device *device,
                               const char *path );
typedef int (*dbusfs_ls_func)( struct dbusfs_device *device, const char *path,
                               char ***list, unsigned long *size );
typedef int (*dbusfs_mkdir_func)( struct dbusfs_device *device,
                                  const char *path );
typedef int (*dbusfs_rmdir_func)( struct dbusfs_device *device,
                                  const char *path );
typedef int (*dbusfs_mv_func)( struct dbusfs_device *device,
                               const char *path, const char *new );
typedef struct dbusfs_device *(*dbusfs_probe_func)( LibHalContext *hal_ctx,
                                                    const char *udi );
typedef void (*dbusfs_remove_func)( struct dbusfs_device *device );

struct dbusfs_file_operations
{
  dbusfs_get_func     get;
  dbusfs_put_func     put;
  dbusfs_rm_func      rm;
  dbusfs_ls_func      ls;
  dbusfs_mkdir_func   mkdir;
  dbusfs_rmdir_func   rmdir;
  dbusfs_mv_func      mv;
  dbusfs_probe_func   probe;
  dbusfs_remove_func  remove;
};

int dbusfs_server( const char *name, struct dbusfs_file_operations *fops );

#endif
