//#define DBUS_API_SUBJECT_TO_CHANGE wankfactor

#include <glib/gmain.h>
#include <dbus/dbus-glib-lowlevel.h>
#include <dbus/dbus.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "dbusfs-hal.h"

#include "dbusfs-server.h"

#define debugf(x) printf x

DBusConnection *system_bus;
struct dbusfs_device *dbusfs_device_list;
char *object_path;

/* rules:
 *
 * path may start with a single / which is ignored
 * path must not end with / (accepting that the first / is ignored)
 * path must not contain '..' as a component
 * path must not contain '.' as a component
 * path must not contain '' as a component (ie: two consecutive slashes)
 *
 * returns 1 if the path is invalid
 */
static int
check_path_name( const char *path )
{
  char *component;
  char *t, *t0;

  if( path[0] == '/' )
    path++;

  if( path[0] == '\0' )
    return 0;

  t = t0 = g_strdup( path );

  while( (component = strsep( &t, "/" )) )
  {
    if( !strcmp( component, ".." ) ||
        !strcmp( component, "." ) ||
        !strcmp( component, "" ) )
    {
      g_free( t0 );

      return 1;
    }
  }

  g_free( t0 );

  return 0;
}

static DBusHandlerResult
dbusfs_handle_root_request( DBusMessage *message, const char *method )
{
  struct dbusfs_device *device;
  DBusMessageIter iter, array;
  DBusMessage *reply;
  char *tmp;

  if( strcmp( method, "ls" ) )
    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED; /* XXX actual error */

  reply = dbus_message_new_method_return( message );
  dbus_message_iter_init_append( reply, &iter );
  dbus_message_iter_open_container( &iter, DBUS_TYPE_ARRAY,
                                    DBUS_TYPE_STRING_AS_STRING, &array );

  for( device = dbusfs_device_list; device; device = device->next )
  {
    tmp = g_strdup_printf( "%s/", device->name );
    dbus_message_iter_append_basic( &array, DBUS_TYPE_STRING, &tmp );
    g_free( tmp );
  }

  dbus_message_iter_close_container( &iter, &array );

  dbus_connection_send( system_bus, reply, NULL );
  dbus_message_unref( reply );

  return DBUS_HANDLER_RESULT_HANDLED;
}

/* takes: a string in the form "/top/rest/path" or "top/rest/path"
 *   returns: "top"
 *            in path: "/rest/path"
 *
 * takes: "foo" or "/foo" or "foo/" or "/foo/"
 *   returns: "foo"
 *            in path: "/"
 *
 * takes: "" or "/"
 *   return: ""
 *           in path: NULL
 *
 * The return value must be free()d.  The new value of path must not be.
 * The new value of path is valid for as long as the old value of path is.
 */
static char *
dbusfs_get_top_level( const char **path )
{
  char *str, *top;

  if( **path == '/' )
    (*path)++;

  if( (str = strchr( *path, '/' )) )
  {
    top = g_strndup( *path, str - *path );
    *path = str;
  }
  else
  {
    top = g_strdup( *path );

    if( *top )
      *path = "/";
    else
      *path = NULL;
  }

  return top;
}

static DBusHandlerResult
dbusfs_handle_request( DBusMessage *message, const char *method,
                       struct dbusfs_file_operations *fops, const char *path )
{
  struct dbusfs_device *device;
  const char *devicename = path;
  DBusMessage *reply;
  char **list;
  int prefix;
  char *data;
  unsigned long size;
  int i;

  devicename = dbusfs_get_top_level( &path );

  printf( "searching for '%s'\n", devicename );

  for( device = dbusfs_device_list; device; device = device->next )
    if( !strncmp( device->name, devicename, prefix ) )
      break;

  printf( "device is %p\n", device );
  printf( "final path is '%s'\n", path );

  if( device == NULL )
    return DBUS_HANDLER_RESULT_NOT_YET_HANDLED; /* XXX real error */

  if( !strcmp( method, "get" ) )
  {
    fops->get( device, path, &data, &size );
    reply = dbus_message_new_method_return( message );
    dbus_message_append_args( reply, DBUS_TYPE_ARRAY, DBUS_TYPE_BYTE, &data, size, DBUS_TYPE_INVALID );
    dbus_connection_send( system_bus, reply, NULL );
    dbus_message_unref( reply );

    printf( "done sending %ld bytes\n", size );

    g_free( data );
  }
  else if( !strcmp( method, "put" ) )
  {
    data = NULL;
    size = 0;

    fops->put( device, path, data, size );
  }
  else if( !strcmp( method, "rm" ) )
  {
    if( fops->rm( device, path ) )
    {

    }
    else
    {
      reply = dbus_message_new_method_return( message );
      dbus_connection_send( system_bus, reply, NULL );
      dbus_message_unref( reply );
    }
  }
  else if( !strcmp( method, "ls" ) )
  {
    fops->ls( device, path, &list, &size );

    reply = dbus_message_new_method_return( message );
    dbus_message_append_args( reply, DBUS_TYPE_ARRAY, DBUS_TYPE_STRING, &list, size, DBUS_TYPE_INVALID );
    dbus_connection_send( system_bus, reply, NULL );
    dbus_message_unref( reply );

    for( i = 0; i < size; i++ )
      g_free( list[i] );
    g_free( list );

    return DBUS_HANDLER_RESULT_HANDLED;
  }
  else if( !strcmp( method, "mkdir" ) )
  {
    fops->mkdir( device, path );
  }
  else if( !strcmp( method, "rmdir" ) )
  {
    data = NULL;
    fops->rmdir( device, path );
  }
  else if( !strcmp( method, "mv" ) )
  {
    data = NULL;
    fops->mv( device, path, data );
  }

  return DBUS_HANDLER_RESULT_HANDLED;
}


static DBusHandlerResult
message_function( DBusConnection *c, DBusMessage *message, void *context )
{
  struct dbusfs_file_operations *fops;
  const char *path, *method;

  fops = (struct dbusfs_file_operations *) context;

  debugf(( "\nGot call: %s %s %s\n", dbus_message_get_interface( message ),
                                     dbus_message_get_path( message ),
                                     dbus_message_get_member( message ) ));

  method = dbus_message_get_member( message );
  //path = dbus_message_get_path( message );
  if( dbus_message_get_args( message, NULL, DBUS_TYPE_STRING, &path, DBUS_TYPE_INVALID ) == FALSE )
  {
    printf( "LAME\n" );
  }


  printf( "method[%s] path[%s]\n", method, path );
  if( path[0] == '\0' || (path[0] == '/' && path[1] == '\0') )
    return dbusfs_handle_root_request( message, method );
  else if( path[0] == '/' && path[1] != '\0' )
    return dbusfs_handle_request( message, method, fops, path + 1 );

  return DBUS_HANDLER_RESULT_NOT_YET_HANDLED; /* XXX actual error */
}

DBusObjectPathVTable vtable = { .message_function = message_function };

int dbusfs_server( const char *name, struct dbusfs_file_operations *fops )
{
  GMainLoop *loop;
  DBusError err;
  char *service;

  dbusfs_hal_init( fops );

  dbus_error_init( &err );

  loop = g_main_loop_new( NULL, FALSE );
  dbus_connection_set_change_sigpipe( TRUE );
  system_bus = dbus_bus_get( DBUS_BUS_SYSTEM, NULL );
  dbus_connection_setup_with_g_main( system_bus, NULL );

  service = g_strdup_printf( "ca.desrt.dbusfs.%s", name );
  dbus_bus_request_name( system_bus, service, 0, &err );
  g_free( service );

  object_path = g_strdup_printf( "/ca/desrt/dbusfs/%s", name );
  dbus_connection_register_object_path( system_bus, object_path,
                                        &vtable, fops );

  if( dbus_error_is_set( &err ) )
    printf( "aquire error: %s (don't expect this to work...)\n", err.message );


  g_main_loop_run( loop );

  return 0;
}
