#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <sys/types.h>
#include <unistd.h>

#include <glib.h>

#include <libhal.h>
#include <libhal-storage.h>
#include <dbus/dbus-glib.h>
#include <dbus/dbus-glib-lowlevel.h>

#include "dbusfs-server.h"

#include "dbusfs-hal.h"

extern struct dbusfs_device *dbusfs_device_list;

#if 0
static void
_hal_mainloop_integration (LibHalContext *ctx, 
			   DBusConnection * dbus_connection)
{
        dbus_connection_setup_with_g_main (dbus_connection, NULL);
}
#endif

static void 
_hal_device_added (LibHalContext *hal_ctx, 
		   const char *udi)
{
  struct dbusfs_file_operations *fops = libhal_ctx_get_user_data( hal_ctx );
  struct dbusfs_device *device;
 
  printf( "added: %s\n", udi );

  /* evil heuristic to remove multiple reports of same device */
  if( g_str_has_suffix( udi, "_0" ) )
    return;

  if( (device = fops->probe( hal_ctx, udi )) )
  {
    device->next = dbusfs_device_list;
    dbusfs_device_list = device;
    device->udi = g_strdup( udi );
  }
}

static void 
_hal_device_removed (LibHalContext *hal_ctx, const char *udi)
{
  struct dbusfs_file_operations *fops = libhal_ctx_get_user_data( hal_ctx );
  struct dbusfs_device **device, *d;
  char *uditmp;

  printf("removed: %s\n", udi );

  for( device = &dbusfs_device_list; *device; device = &(*device)->next )
    if( !strcmp( udi, (*device)->udi ) )
      break;

  /* grab the device */
  if( (d = *device) == NULL )
    return;

  /* take it out of the list */
  *device = (*device)->next;

  /* free the structures */
  uditmp = d->udi;

  if( fops->remove )
    fops->remove( d );
  else
    free( d );

  g_free( uditmp );
}

void
dbusfs_hal_init( struct dbusfs_file_operations *fops )
{
  DBusConnection *connection;
  LibHalContext *ctx;

  connection = dbus_bus_get( DBUS_BUS_SYSTEM, NULL );

  ctx = libhal_ctx_new();
  libhal_ctx_set_device_added( ctx, _hal_device_added );
  libhal_ctx_set_device_removed( ctx, _hal_device_removed );
  libhal_ctx_set_dbus_connection( ctx, connection );
  libhal_ctx_set_user_data( ctx, fops );
  libhal_ctx_init( ctx, NULL );

  printf( "hal be with you\n" );
}
