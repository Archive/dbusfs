#ifndef _libdbusfs_dbusfs_methods_h_
#define _libdbusfs_dbusfs_methods_h_

int dbusfs_get( const char *path, char **contents, unsigned long *size );
int dbusfs_put( const char *path, char *contents, unsigned long size );
int dbusfs_rm( const char *path );
int dbusfs_ls( const char *path, char ***list, unsigned long *size );
int dbusfs_mkdir( const char *path );
int dbusfs_mv( const char *path, const char *new );
int dbusfs_rmdir( const char *path );

#endif
