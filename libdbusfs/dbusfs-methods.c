#define DBUS_API_SUBJECT_TO_CHANGE wankfactor

#include <dbus/dbus-glib-lowlevel.h>
#include <dbus/dbus.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "dbusfs-methods.h"

static DBusConnection *system_bus;
GSList *providers;

static void
initialise_dbusfs_providers( void )
{
  DBusMessageIter iter, array;
  DBusMessage *message;
  char *tmp;

  message = dbus_message_new_method_call( DBUS_SERVICE_DBUS,
                                          DBUS_PATH_DBUS,
                                          DBUS_INTERFACE_DBUS,
                                          "ListNames" );

  message = dbus_connection_send_with_reply_and_block( system_bus, message,
                                                       60000, NULL );

  dbus_message_iter_init( message, &iter );
  if( dbus_message_iter_get_arg_type( &iter ) != DBUS_TYPE_ARRAY ||
      dbus_message_iter_get_element_type( &iter ) != DBUS_TYPE_STRING )
  {
    return;
  }

  dbus_message_iter_recurse( &iter, &array );

  while (dbus_message_iter_get_arg_type( &array ) == DBUS_TYPE_STRING )
  {
    dbus_message_iter_get_basic( &array, &tmp );

    if( g_str_has_prefix( tmp, "ca.desrt.dbusfs." ) )
    {
      //printf( "%s\n", tmp );

      tmp += strlen( "ca.desrt.dbusfs." );
      providers = g_slist_prepend( providers, g_strdup( tmp ) );
    }

    dbus_message_iter_next( &array );
  }

  dbus_message_unref( message );
}

static void
dbusfs_initialise( void )
{
  if( system_bus )
    return;

 // dbus_connection_set_change_sigpipe( TRUE );
  system_bus = dbus_bus_get( DBUS_BUS_SYSTEM, NULL );
  //dbus_connection_setup_with_g_main( system_bus, NULL );

  initialise_dbusfs_providers();
}

/* takes: a string in the form "/top/rest/path" or "top/rest/path"
 *   returns: "top"
 *            in path: "/rest/path"
 *
 * takes: "foo" or "/foo" or "foo/" or "/foo/"
 *   returns: "foo"
 *            in path: "/"
 *
 * takes: "" or "/"
 *   return: ""
 *           in path: NULL
 *
 * The return value must be free()d.  The new value of path must not be.
 * The new value of path is valid for as long as the old value of path is.
 */
static char *
dbusfs_get_top_level( const char **path )
{
  char *str, *top;

  if( **path == '/' )
    (*path)++;

  if( (str = strchr( *path, '/' )) )
  {
    top = g_strndup( *path, str - *path );
    *path = str;
  }
  else
  {
    top = g_strdup( *path );

    if( *top )
      *path = "/";
    else
      *path = NULL;
  }

  return top;
}

static DBusMessage *
dbusfs_new_method_call( const char *method, const char *path,
                        DBusMessageIter *iter )
{
  DBusMessageIter local_iter;
  DBusMessage *call;
  char *server;
  char *file;
  char *top;

  if( !iter )
    iter = &local_iter;

  top = dbusfs_get_top_level( &path );
  server = g_strdup_printf( "ca.desrt.dbusfs.%s", top );
  file = g_strdup_printf( "/ca/desrt/dbusfs/%s", top );
  g_free( top );

  //printf("method[%s] interface[%s] server[%s] file[%s]\n", method, "ca.desrt.dbusfs", server, file );
  call = dbus_message_new_method_call( server, file,
                                       "ca.desrt.dbusfs", method );
  g_free( server );
  g_free( file );

  dbus_message_iter_init_append( call, iter );
  dbus_message_iter_append_basic( iter, DBUS_TYPE_STRING, &path );

  return call;
}

static DBusMessage *
dbusfs_make_call( DBusMessage *message )
{
  DBusError error;
  dbus_error_init( &error );

  /* http://freedesktop.org/bugzilla/show_bug.cgi?id=857 */
#ifdef DBUS_BUG_857_FIXED
  return dbus_connection_send_with_reply_and_block( system_bus, message,
                                                    60000, NULL );
#else
  G_LOCK_DEFINE_STATIC(dbuslock);
  DBusMessage *m;
  G_LOCK(dbuslock);
  m = dbus_connection_send_with_reply_and_block( system_bus, message,
                                                 60000, &error );

  if( !m )
  {
    printf( "error: %s (%s)\n", error.name, error.message );
    exit( 1 );
  }

  G_UNLOCK(dbuslock);
  return m;
#endif
}

static int
dbusfs_get_reply( DBusMessage *message, int first_arg_type, ... )
{
  DBusMessage *reply;
  DBusError error;
  va_list ap;

  dbus_error_init( &error );

  if( (reply = dbusfs_make_call( message )) == NULL )
    return 1;

  va_start( ap, first_arg_type );

  dbus_message_get_args_valist( reply, &error, first_arg_type, ap );
  dbus_message_unref( reply );

  if( dbus_error_is_set( &error ) )
  {
    printf( "get_reply: error: %s (%s)\n", error.name, error.message );
    dbus_error_free( &error );
    return 1;
  }

  return 0;
}

int dbusfs_get( const char *path, char **contents, unsigned long *size )
{
  DBusMessage *message;
  int result;

  dbusfs_initialise();

  message = dbusfs_new_method_call( "get", path, NULL );
  result = dbusfs_get_reply( message, DBUS_TYPE_ARRAY, DBUS_TYPE_BYTE,
                             contents, size, DBUS_TYPE_INVALID );

  if( result == 0 )
    *contents = g_memdup( *contents, *size );

  return result;
}

int dbusfs_put( const char *path, char *contents, unsigned long size )
{
  DBusMessageIter iter, array;
  DBusMessage *message;

  dbusfs_initialise();

  message = dbusfs_new_method_call( "put", path, &iter );
  dbus_message_iter_open_container( &iter, DBUS_TYPE_ARRAY,
                                    DBUS_TYPE_BYTE_AS_STRING, &array );
printf("putting size %ld\n", size );
  dbus_message_iter_append_fixed_array( &array, DBUS_TYPE_BYTE,
                                        &contents, size );
  dbus_message_iter_close_container( &iter, &array );

  return dbusfs_get_reply( message, DBUS_TYPE_INVALID );
}

int dbusfs_rm( const char *path )
{
  DBusMessage *message;

  dbusfs_initialise();

  message = dbusfs_new_method_call( "rm", path, NULL );
  return dbusfs_get_reply( message, DBUS_TYPE_INVALID );
}

static int dbusfs_root_ls( char ***list, unsigned long *size )
{
  int i;

  *size = g_slist_length( providers );

  *list = g_malloc( *size * sizeof (char *) );

  /* this is O(n^2) when it could be O(n), but n is very small */
  for( i = 0; i < *size; i++ )
    (*list)[i] = g_strdup_printf( "%s/", (char *) g_slist_nth_data( providers, i ) );

  return 0;
}

int dbusfs_ls( const char *path, char ***list, unsigned long *size )
{
  DBusMessage *message;

  dbusfs_initialise();

  if( !strcmp( path, "/" ) )
    return dbusfs_root_ls( list, size );

  message = dbusfs_new_method_call( "ls", path, NULL );
  return dbusfs_get_reply( message, DBUS_TYPE_ARRAY, DBUS_TYPE_STRING,
                           list, size, DBUS_TYPE_INVALID );
}

int dbusfs_mkdir( const char *path )
{
  DBusMessage *message;

  dbusfs_initialise();

  message = dbusfs_new_method_call( "mkdir", path, NULL );
  return dbusfs_get_reply( message, DBUS_TYPE_INVALID );
}

int dbusfs_mv( const char *path, const char *new )
{
  DBusMessageIter iter;
  DBusMessage *message;

  dbusfs_initialise();

  message = dbusfs_new_method_call( "mv", path, &iter );
  dbus_message_iter_append_basic( &iter, DBUS_TYPE_STRING, &new );

  return dbusfs_get_reply( message, DBUS_TYPE_INVALID );
}

int dbusfs_rmdir( const char *path )
{
  DBusMessage *message;

  dbusfs_initialise();

  message = dbusfs_new_method_call( "rmdir", path, NULL );
  return dbusfs_get_reply( message, DBUS_TYPE_INVALID );
}
