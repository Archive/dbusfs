#include <stdio.h>
#include <dbusfs.h>

int main( int argc, char **argv )
{
  if( argc != 3 )
  {
    fprintf( stderr, "syntax: dbusfs-mv /path/old /path/new\n" );
    return 1;
  }

  if( dbusfs_mv( argv[1], argv[2] ) )
  {
    fprintf( stderr, "Failure.\n" );
    return 1;
  }

  return 0;
}
