#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dbusfs.h>

int main( int argc, char **argv )
{
  unsigned long length;
  char *contents;
  int s = 0;

  if( argc != 2 )
  {
    fprintf( stderr, "syntax: dbusfs-put /path (input is from stdin)\n" );
    return 1;
  }

  contents = NULL;
  length = 0;

  do
  {
    length += s;

    contents = realloc( contents, length + 0x100000 );
  }
  while( (s = read( 0, contents + length, 0x100000 )) > 0 );

  if( dbusfs_put( argv[1], contents, length ) )
  {
    fprintf( stderr, "Failure.\n" );
    return 1;
  }

  free( contents );

  return 0;
}
