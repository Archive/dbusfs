#include <stdio.h>
#include <stdlib.h>
#include <dbusfs.h>

int main( int argc, char **argv )
{
  unsigned long length;
  char **list;
  int i;

  if( argc != 2 )
  {
    fprintf( stderr, "syntax: dbusfs-ls /path\n" );
    return 1;
  }

  if( dbusfs_ls( argv[1], &list, &length ) )
  {
    fprintf( stderr, "Failure.\n" );
    return 1;
  }

  for( i = 0; i < length; i++ )
  {
    printf( "%s\n", list[i] );
    free( list[i] );
  }

  free( list );

  return 0;
}
