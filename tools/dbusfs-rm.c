#include <stdio.h>
#include <dbusfs.h>

int main( int argc, char **argv )
{
  if( argc != 2 )
  {
    fprintf( stderr, "syntax: dbusfs-rm /path\n" );
    return 1;
  }

  if( dbusfs_rm( argv[1] ) )
  {
    fprintf( stderr, "Failure.\n" );
    return 1;
  }

  return 0;
}
