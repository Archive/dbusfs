#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dbusfs.h>

int main( int argc, char **argv )
{
  unsigned long length;
  char *contents;

  if( argc != 2 )
  {
    fprintf( stderr, "syntax: dbusfs-get /path (output is to stdout)\n" );
    return 1;
  }

  if( dbusfs_get( argv[1], &contents, &length ) )
  {
    fprintf( stderr, "Failure.\n" );
    return 1;
  }

  write( 1, contents, length );

  free( contents );

  return 0;
}
