#include <stdio.h>
#include <dbusfs.h>

int main( int argc, char **argv )
{
  if( argc != 2 )
  {
    fprintf( stderr, "syntax: dbusfs-mkdir /path\n" );
    return 1;
  }

  if( dbusfs_mkdir( argv[1] ) )
  {
    fprintf( stderr, "Failure.\n" );
    return 1;
  }

  return 0;
}
