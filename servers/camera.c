#include <gphoto2/gphoto2.h>
#include <glib.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "dbusfs-server.h"
#define debugf(x) printf x

static CameraAbilities*
camera_lookup_usb_ids( int vendor, int product )
{
  static CameraAbilitiesList *list;
  static CameraAbilities abilities;
  static int count;
  int i;

  if( !list )
  {
    gp_abilities_list_new( &list );
    gp_abilities_list_load( list, NULL );
    count = gp_abilities_list_count( list );
  }

  for( i = 0; i < count; i++ )
  {
    gp_abilities_list_get_abilities( list, i, &abilities );

    if( abilities.usb_vendor == vendor && abilities.usb_product == product )
      return &abilities;
  }

  return NULL;
}

static int
camera_split_pathname( const char *path, char **r_folder, char **r_file )
{
  char *file;

  if( path[0] != '/' )
    return 1;

  file = rindex( path, '/' ); /* will never fail */

  if( file[1] == '\0' )
    return 1;

  if( file == path )
    *r_folder = g_strdup( "/" ); /* give "/" instead of "" */
  else
    *r_folder = g_strndup( path, file - path );

  *r_file = g_strdup( file + 1 );

  return 0;
}

static Camera *
get_camera_from_numeric( int bus, int device )
{
  GPPortInfoList *infolist;
  Camera *camera = NULL;
  GPPortInfo info;
  char *portname;
  int port = -1;
  int i, n;

  if( (portname = g_strdup_printf( "usb:%03d,%03d", bus, device )) == NULL )
    return NULL;

  if( gp_port_info_list_new( &infolist ) < 0 )
    goto free_portname;

  if( gp_port_info_list_load( infolist ) )
    goto free_infolist;

  if( (port = gp_port_info_list_lookup_path( infolist, portname )) < 0 )
    goto free_infolist;

  if( gp_port_info_list_get_info( infolist, port, &info ) )
    goto free_infolist;

  if( gp_camera_new( &camera ) )
    goto free_infolist;

  if( gp_camera_set_port_info( camera, info ) )
  {
    gp_camera_free( camera );
    camera = NULL;
  }

  free_infolist:
  gp_port_info_list_free( infolist );

  free_portname:
  g_free( portname );

  return camera;
}

int camera_get( struct dbusfs_device *device, const char *path,
                char **contents, unsigned long *size )
{
  Camera *camera = device->private;
  CameraFile *camerafile;
  char *folder, *file;
  const char *data;
  int err;

  if( camera_split_pathname( path, &folder, &file ) )
    return 1; /* XXX file not found */

  gp_file_new( &camerafile );
  err = gp_camera_file_get( camera, folder, file, GP_FILE_TYPE_NORMAL,
                          camerafile, NULL );

  if( !err )
  {
    gp_file_get_data_and_size( camerafile, &data, size );
    *contents = g_memdup( data, *size );
    gp_file_free( camerafile );
  }

  g_free( folder );
  g_free( file );

  if( err )
    return 1; /* XXX file not found */
  else
    return 0;
}

int camera_put( struct dbusfs_device *device, const char *path,
                char *contents, unsigned long size )
{
  printf( "put %s\n", path );
  return 1;
}

int camera_rm( struct dbusfs_device *device, const char *path )
{
  Camera *camera = device->private;
  char *folder, *file;
  int err;

  if( camera_split_pathname( path, &folder, &file ) )
    return 1; /* XXX file not found */

  err = gp_camera_file_delete( camera, folder, file, NULL );

  g_free( folder );
  g_free( file );

  if( err )
    return 1; /* XXX file not found */
  else
    return 0;
}

int camera_ls( struct dbusfs_device *device,
               const char *path, char ***list, unsigned long *size )
{
  Camera *camera = device->private;
  CameraList folders, files;
  int nfolders, nfiles, i;
  const char *name;

  if( gp_camera_folder_list_folders( camera, path, &folders, NULL ) ||
      gp_camera_folder_list_files( camera, path, &files, NULL ) )
  {
    return 1; /* no such file error XXX */
  }

  nfolders = gp_list_count( &folders );
  nfiles = gp_list_count( &files );
  *size = nfolders + nfiles;

  *list = malloc( (nfolders + nfiles) * sizeof (char *) );

  for( i = 0; i < nfolders; i++ )
  {
    gp_list_get_name( &folders, i, &name );
    (*list)[i] = g_strdup_printf( "%s/", name );
  }

  for( i = 0; i < nfiles; i++ )
  {
    gp_list_get_name( &files, i, &name );
    (*list)[i + nfolders] = g_strdup( name );
  }

  return 0;
}

int camera_mkdir( struct dbusfs_device *device, const char *path )
{
  printf( "mkdir %s\n", path );
  return 1;
}

int camera_rmdir( struct dbusfs_device *device, const char *path )
{
  printf( "rmdir %s\n", path );
  return 1;
}

int camera_mv( struct dbusfs_device *device,
               const char *path, const char *new )
{
  printf( "mv %s\n", path );
  return 1;
}

struct dbusfs_device *camera_probe( LibHalContext *hal_ctx, const char *udi )
{
  struct dbusfs_device *device;
  CameraAbilities *abilities;
  Camera *camera;
  int prod, vend;
  int bus_id, dev_id;

  prod = libhal_device_get_property_int( hal_ctx, udi, "usb_device.product_id", NULL );
  vend = libhal_device_get_property_int( hal_ctx, udi, "usb_device.vendor_id", NULL );

  printf( "camera:: new device is %x/%x\n", vend, prod );

  abilities = camera_lookup_usb_ids( vend, prod );

  if( abilities == NULL )
    return NULL;

  printf( "Got camera: %s\n", abilities->model );
  printf( "  ops: %04x  fops: %04x  Fops: %04x\n\n", abilities->operations,
          abilities->file_operations, abilities->folder_operations );

  dev_id = libhal_device_get_property_int( hal_ctx, udi, "usb_device.linux.device_number", NULL );
  bus_id = libhal_device_get_property_int( hal_ctx, udi, "usb_device.bus_number", NULL );

  if( (camera = get_camera_from_numeric( bus_id, dev_id )) == NULL )
  {
    printf( "unable to get device at %03d/%03d!\n" , bus_id, dev_id );
    return NULL;
  }

  printf( "Got camera device!!\n" );

  gp_camera_init( camera, NULL );

  device = malloc( sizeof (struct dbusfs_device) );
  device->name = g_strdup( abilities->model );
  device->next = NULL;
  device->private = camera;

  printf( "Device added!!" );

  return device;
}

static void
camera_remove( struct dbusfs_device *device )
{
  Camera *camera = device->private;

  printf( "Removing '%s'\n", device->name );

  free( device->name );
  gp_camera_free( camera );
  free( device );
}

struct dbusfs_file_operations camera_fops = {
  .get    = camera_get,
  .put    = camera_put,
  .rm     = camera_rm,
  .ls     = camera_ls,
  .mkdir  = camera_mkdir,
  .rmdir  = camera_rmdir,
  .mv     = camera_mv,
  .probe  = camera_probe,
  .remove = camera_remove
};

int main( void )
{
  return dbusfs_server( "camera", &camera_fops );
}
