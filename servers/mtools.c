#include <glib.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

#include "dbusfs-server.h"
#define debugf(x) printf x

int mtools_pipe( struct dbusfs_device *device, const char **command,
                 int pipe_direction, const char *path )
{
  int pipefds[2];

  if( pipe_direction )
    pipe( pipefds );

printf( "forking %s %s %s ::%s\n", command[0], command[1], device->private, path );

  if( fork() == 0 )
  {
    char **cmd;
    int i;

    for( i = 0; command[i]; i++ );

    cmd = g_memdup( command, (i + 3) * sizeof (char *) );
    cmd[i] = device->private;
    cmd[i+1] = g_strdup_printf( "::%s", path );
    cmd[i+2] = NULL;
 
    if( pipe_direction < 0 )
    {
      dup2( pipefds[1], 1 );
      close( pipefds[0] );
    }

    if( pipe_direction > 0 )
    {
      dup2( pipefds[0], 0 );
      close( pipefds[1] );
    }

    execvp( cmd[0], cmd );
    exit( 1 );
  }

  if( pipe_direction < 0 )
  {
    close( pipefds[1] );
    return pipefds[0];
  }

  if( pipe_direction > 0 )
  {
    close( pipefds[0] );
    return pipefds[1];
  }

  return 0;
}

int mtools_get( struct dbusfs_device *device, const char *path,
                char **contents, unsigned long *size )
{
  static const char *cmd[] = { "mtype", "-i", NULL };
  int fd = mtools_pipe( device, cmd, -1, path );
  int s = 0;

  *contents = NULL;
  *size = 0;

  do
  {
    *size += s;

    *contents = g_realloc( *contents, *size + 0x100000 );
  }
  while( (s = read( fd, *contents + *size, 0x100000 )) > 0 );

  return 0;
}

int mtools_put( struct dbusfs_device *device, const char *path,
                char *contents, unsigned long size )
{
  static const char *cmd[] = { "mcopy", "-", "-oi", NULL };
  int fd = mtools_pipe( device, cmd, 1, path );

  write( fd, contents, size );
  close( fd );

  return 0;
}

int mtools_rm( struct dbusfs_device *device, const char *path )
{
  static const char *cmd[] = { "mdel", "-i", NULL };
  mtools_pipe( device, cmd, 0, path );

  return 0;
}

int mtools_ls( struct dbusfs_device *device,
               const char *path, char ***list, unsigned long *size )
{
  static const char *cmd[] = { "mdir", "-fbi", NULL };
  int fd = mtools_pipe( device, cmd, -1, path );
  FILE *fp = fdopen( fd, "r" );
  char buffer[2048], *str;

  *list = NULL;
  *size = 0;

  while( fgets( buffer, 2048, fp ) )
  {

    if( buffer[0] != ':' || buffer[1] != ':' ||
        buffer[2] != '/' || buffer[3] == '\0' )
    {
      g_free( *list );
      *size = 0;

      return 1;
    }

    if( (str = strchr( buffer, '\n' )) )
      *str = '\0';

    *list = g_realloc( *list, (*size + 1) * sizeof (char *) );
    (*list)[*size] = g_strdup( buffer + 3 );
    (*size)++;
  }

  fclose( fp );

  return 0;
}

int mtools_mkdir( struct dbusfs_device *device, const char *path )
{
  static const char *cmd[] = { "mmd", "-i", NULL };
  mtools_pipe( device, cmd, 0, path );

  return 0;
}

int mtools_rmdir( struct dbusfs_device *device, const char *path )
{
  static const char *cmd[] = { "mrd", "-i", NULL };
  mtools_pipe( device, cmd, 0, path );

  return 0;
}

int mtools_mv( struct dbusfs_device *device,
               const char *path, const char *new )
{
  printf( "mv %s\n", path );
  return 1;
}

struct dbusfs_device *mtools_probe( LibHalContext *hal_ctx, const char *udi )
{
  struct dbusfs_device *device;

  device = malloc( sizeof (struct dbusfs_device) );
  device->name = g_strdup( "MP3 Player" );
  device->next = NULL;
  device->private = g_strdup( "/home/desrt/foo" );

  return device;
}

struct dbusfs_file_operations mtools_fops = {
  .get   = mtools_get,
  .put   = mtools_put,
  .rm    = mtools_rm,
  .ls    = mtools_ls,
  .mkdir = mtools_mkdir,
  .rmdir = mtools_rmdir,
  .mv    = mtools_mv,
  .probe = mtools_probe
};

int main( void )
{
  return dbusfs_server( "mtools", &mtools_fops );
}
